var elixir = require('laravel-elixir');

//Require Sass Compass Addon
require('laravel-elixir-sass-compass');

//Easy Versioning without the Subfolder
require('elixir-busting');



//Build
elixir(function(mix) {

	//The Assets Directory
	var Assets = 'resources/assets/',

	//The SASS Directory
	    SASS = 'resources/assets/sass/';

    //Render
    mix.compass('app.scss', 'public/assets/css', {
    	require: 	['sass-globbing'],
	    sass: 		SASS
    });

    //Copy Fonts
    mix.copy( SASS + 'fonts/', 'public/assets/fonts/' );

    //Copy Images
    mix.copy( Assets + 'images/', 'public/assets/images/' );

    //Copy Javascript
    mix.copy( Assets + 'js/', 'public/assets/js/' );

    //Version Files
    /*mix.busting([
        //'public/assets/css/app.css' ,

        //'public/assets/js/general.js' ,

        //'public/assets/js/app/app.js' ,

    ]);*/

});
